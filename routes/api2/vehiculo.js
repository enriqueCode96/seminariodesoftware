var express = require('express');
var router = express.Router();
//libreria de mysql
var mysql = require('mysql');
const jwt = require('jsonwebtoken')
const secret_key = process.env.secret_key || null
const bcrypt = require('bcryptjs')

var con = mysql.createPool({
    host: "localhost",
    user: "root",
    password: "Dragon97",
    database: "vehiculos",
    insecureAuth: true,
    multipleStatements: true
})

//creo la api para que me devuelva toda la informacion
router.get('/get_vehiculos', (req, res, next)=>{

   
    //mantiene la sentencia que mando ala base
    con.query('SELECT * FROM vehiculos', (err, result, field) =>{
        if(err){
            next(err)
        }else{
            res.status(200).json(result)
        }
    } )
})


// Devulve unvehiculo consulta por placa, devuelve el vehiculo
router.get('/get_vehiculo', (req, res, next)=>{
    //mantiene la sentencia que mando ala base
    var query = 'SELECT * FROM vehiculos where placa = ?' 

    // es el dato que se envia a buscar (placa) 
    var values = [req.query.placa]
    /*
    con.query(query, values, (err, result, field) =>{
        if(err){
            next(err)
        }else{
            res.status(200).json(result)
        }
    }) */

    con.query('SELECT * FROM vehiculos where placa = ?', [values], (err, result, field) =>{
        if(err){
            next(err)
        }else{
            res.status(200).json(result)
        }
    })

})

//todo: Insertar un nuevo vehiculo
router.post('/insert_vehiculo', (req, res, next) =>{
    
    let valores_agregado = [req.body.placa.toUpperCase(), 
                            req.body.color.toUpperCase(),
                            req.body.marca.toUpperCase(),
                            req.body.modelo.toUpperCase(),
                            //req.body.ciudad.toUpperCase()
                            ] 

    con.query( 'INSERT INTO vehiculos (placa, color, marca, modelo) VALUES ( ?, ? , ? ,?)', valores_agregado, (err, result, field) =>{
        if (err) {
            next(err)
        }else{
            res.send({status: 'OK', message: 'Se ha insertado un vehiculo nuevo!'})
            //res.status(200).json(result)
        }
    })
} )

//todo eliminar un vehiculo
router.delete('/delete_vehiculo', (req, res, next) =>{
    let valor_borrado = [req.query.placa.toUpperCase()]
    con.query('DELETE FROM vehiculos WHERE placa = ?', [valor_borrado], (error, result, field) =>{
        if (error) throw error
        res.send({status: 'OK', message: 'Vehiculo borrado exitosamente'})
    })
})


router.put('/update_vahiculo2', (req, res, next) =>{

    const  {placaBus} = req.params
    const valores = {placa,color,marca,modelo }  = req.body 

    let valores_cambiados = [   req.body.placa.toUpperCase(),
                                req.body.color.toUpperCase(),
                                req.body.marca.toUpperCase(),
                                req.body.modelo.toUpperCase(),
                                //req.body.ciudad.toUpperCase(),
                                req.query.placa.toUpperCase()
                                
                            ]
        con.query('UPDATE vehiculos SET placa = ? ,color = ?, marca = ?, modelo = ? WHERE  placa = ?', [valores, placaBus],(error, result, field) =>{
            
            if (error) {
                return res.json({
                    mensaje: `La placa no existe en la base de datos`
                })    
            }           
            //if (error) throw error
            res.send({status: 'OK', message: 'Veiculo actualizado exitosamente!'})      
        })
        
})



router.get('/getciudades', (req, res) => {
    //mantiene la sentencia que mando ala base
    con.query('SELECT * FROM ciudad', (err, result, field) =>{
        if(err){
            next(err)
        }else{
            res.status(200).json(result)
        }
    } )
})





//todo: modificar vehiculo 
router.put('/update_vehiculo', (req, res, next) =>{
        let consulta_modificar ='UPDATE vehiculos SET color = ?, marca = ?, modelo = ? WHERE  placa = ?'
        //const valores_cambiados = {placa, color, marca, modelo} = req.body
        let valores_cambiados2= [
                                req.query.placa.toUpperCase(),
                                req.body.color.toUpperCase(), 
                                req.body.marca.toUpperCase(), 
                                req.body.modelo.toUpperCase()
                                ]
            
         con.query(consulta_modificar, valores_cambiados2,(err, result, field) =>{
           if (err) {
                next(err)
                res.status(500).send({status: 'ERROR', message: 'NO EXISTE'})       
            }else{
                res.send({status: 'OK', message: 'Vehiculo actualizado'})
            }                        
        })  
})  


//Agregar usuario
router.post('/insert_usuario', (req, res, next) => {
    var user = {
        username: req.body.username,
        password: req.body.password
    };
    const create_user = (user) => {
        console.log(user);
        var query = "INSERT INTO usuarios (username, password) VALUES (?) ";
        con.query(query, [Object.values(user)], (err, result, fields) => {
            if (err) {
                console.log(err);
                res.status(500).send();
            } else {
                res.status(200).send();
            }
        });
    };
    bcrypt.hash(user.password, 10).then((hashedPassword) => {
        user.password = hashedPassword;
        create_user(user);
    });
});


//Login de usuario

router.post('/login', (req,res,next) =>{
    var user = {
        username: req.body.username,
        password: req.body.password
    };
    const get_token = (user) => {
        var query = "SELECT USERNAME, PASSWORD FROM usuarios WHERE username = ?"
        con.query(query, [user.username], (err, result, fields) => {
            if (err || result.length == 0) {
                console.log(err);
                res.status(400).json({message:"Usuario o Contraseña Incorrectos"});
            } else {
                bcrypt.compare(user.password,result[0].PASSWORD, (error, isMatch)=> {
                    if (isMatch){
                        var token = jwt.sign({userId: result[0].id}, secret_key);
                        res.status(200).json({token});
                    }else if (error){
                        res.status(400).json(error);
                    }else {
                        res.status(400).json({message: "Usuario o Contraseña Incorrectos"});
                    }
                });
            }
        });
    }
    get_token(user);

});





module.exports = router