var express = require('express');
var router = express.Router();

const mongoose = require('mongoose');


//objeto del modelo (importo el modelo)
const Carro = require('../../models/carro');

try {
    mongoose.connect('mongodb+srv://esRamirez:padre@enriqueramirez.4ab3t.mongodb.net/test',{
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
    });
    console.log('Conectado a MongoDB')
}catch(err) {
    console.log('Ocurrio un error al conectarse: '+ err)
}


//API para agregar un nuevo carro
router.post('/insert_carro', (req, res) =>{
    try {
        const carro = new Carro(req.body) //Carro es el esquema de Mongo
        carro.save()//guardo el carro a la clase 
        res.status(200).json({resultado: 'Carro Agregado!'})     
    } catch (error) {
        console.log('Error al insertar en Mongo'+error)
    }
})


//Obtener todos los vehiculos registrados
router.get('/get_carros', async (req, res)=>{

    //.find() necesita una asincronizacion, promesa
    const carros = await Carro.find()//traigo todos los registrados
    res.status(200).json(carros)
})

//modificar un carro
router.put('/update_carro', async (req, res) =>{
    
    //agarro el id del carros
    const cid = req.body.id

    //ejecuto la instruccion
    const carroDB = await Carro.findById(cid)
    console.log(carroDB)
    //si no existe ese ID
    if (!carroDB) {
        res.status(404).json({ message: 'no existe el carro'})
    }

    // si existe el carro
    const datos = req.body //me almacena toda la informacion del carro

    //saco la placa  del carro para que no sea modificable
    delete datos.placa
    

    const carroActualizado = await Carro.findByIdAndUpdate(cid,datos,{new:true})

    res.status(200).json({carro: carroActualizado})
})  


//borrar un carro
router.delete('/delete_carro', async(req, res) =>{
    const cid = req.query.id

    const carroDB = await Carro.findById(cid)

    if (!carroDB) {
        res.status(404).json({ message: 'no existe el carro'})
    }

    await Carro.findByIdAndDelete(cid)

    res.status(200).json({message: 'Carro Borrado'})
})




module.exports = router


//esque ma que represata la estructura de mongo
