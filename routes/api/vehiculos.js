var express = require('express');
var router = express.Router();
var VehiculoController = require('../../controllers/api/vehiculoControllerAPI');

router.get('/', VehiculoController.Vehiculo_list);
router.post('/create', VehiculoController.Vehiculo_create);
router.delete('/delete', VehiculoController.Vehiculo_delete);
router.put('/update', VehiculoController.Vehiculo_update);

module.exports = router;