//Creamo nuestro objeto Vehiculo y su constructor
var Vehiculo = function(id, color, marca, modelo){
    this.id = id;
    this.color = color;
    this.marca = marca;
    this.modelo = modelo;
}

//Redefinimos el metodo toString

Vehiculo.prototype.toString = function(){
    return 'Id: ' + this.id + "color: " + this.color;
}

//Cremamos un arreglo para almacenar todos los vehiculos
Vehiculo.allVehiculos = [];

//Creamos un metodo para agregar elementos al arreglo (vehiculos)
Vehiculo.add = function(aVehi){
    Vehiculo.allVehiculos.push(aVehi);
}

//agregar dos vehiculos por default

var a = new Vehiculo(1, 'Negro', 'Ford', 'Escape');
var b = new Vehiculo(2, 'Gris', 'Nissan', 'Sentra');

Vehiculo.add(a)
Vehiculo.add(b)

//eliminar un vehiculo del arreglo mdiante su ID
Vehiculo.RemoveById = function(aVehi){
    for(var i=0; i<Vehiculo.allVehiculos.length; i++){
        if(Vehiculo.allVehiculos[i].id == aVehi){
            Vehiculo.allVehiculos.splice(i,1);
            break;
        }
    }
}

Vehiculo.findById = function(aVehi){
    var aVehi = Vehiculo.allVehiculos.find(x => x.id == aVehi)
    if(aVehi)
        return aVehi
    else
        throw new Error(`No existe un vehiculo con el ID ${aVehi}`);    
}

//Exportamos para que quede disponible
module.exports = Vehiculo;