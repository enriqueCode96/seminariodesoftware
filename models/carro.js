const mongoose = require('mongoose');

const Schema = mongoose.Schema



//objeto vehiculo, esquema que va a utilizar mongo para guardar los datos
const Carro = new Schema({
    placa: String,
    color: String,
    marca: String,
    modelo: String,
})


//creo el modelo, (la tabla vehiculo )
const model = mongoose.model('Vehiculos', Carro)

module.exports = model